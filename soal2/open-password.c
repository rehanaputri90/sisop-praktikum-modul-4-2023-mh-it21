#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

void base64_decode(const char *input, char *output) {
    const char decoding_table[] = {
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
        'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
        'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
        'w', 'x', 'y', 'z', '0', '1', '2', '3',
        '4', '5', '6', '7', '8', '9', '+', '/'
    };

    int index(char c) {
        const char *ptr = strchr(decoding_table, c);
        if (ptr) {
            return ptr - decoding_table;
        } else {
            return -1;
        }
    }

    while (*input) {
        unsigned char sextet_a = index(*input++);
        unsigned char sextet_b = index(*input++);
        unsigned char sextet_c = index(*input++);
        unsigned char sextet_d = index(*input++);

        unsigned char triple = (sextet_a << 2) | (sextet_b >> 4);
        *output++ = triple;

        if (*input != '=' && sextet_b != (unsigned char)-1) {
            triple = ((sextet_b & 0x0F) << 4) | (sextet_c >> 2);
            *output++ = triple;
        }

        if (*input != '=' && sextet_c != (unsigned char)-1) {
            triple = ((sextet_c & 0x03) << 6) | sextet_d;
            *output++ = triple;
        }
    }

    *output = '\0'; 
}

int main() {
    FILE *file;
    const char *file_path = "zip-pass.txt";

    file = fopen(file_path, "r");

    if (file == NULL) {
        return 1; 
    }

    char buffer[100];
    if (fgets(buffer, sizeof(buffer), file) != NULL) {
        buffer[strcspn(buffer, "\n")] = '\0';
        if (strlen(buffer) > 0) {
            buffer[strlen(buffer) - 1] = '\0';
        }
        printf("%s", buffer);
        char decoded_pass[100];
        base64_decode(buffer, decoded_pass);
        if (strlen(decoded_pass) > 0) {
            decoded_pass[strlen(decoded_pass) - 1] = '\0';
        } printf("%s\n", decoded_pass);

        const char *zipFilePath = "home.zip";
        char *args[] = {"unzip", "-P", decoded_pass, (char *)zipFilePath, "-d", "fold", NULL};
        execvp("unzip", args);
        perror("execvp");
        return 1;
    }

    fclose(file);

    return 0;
}