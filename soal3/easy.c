#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <time.h>
#include <sys/time.h>

#define MAX_PATH 255

static const char *mounted_path ="/home/raditsoic/work";

static int is_modular_dir(const char *dirname) 
{
    return strncmp(dirname, "module_", 7) == 0;
}

static void make_log(const char *level, const char *action, const char *desc)
{
    time_t t;
    struct tm *tm_info;
    time(&t);
    tm_info = localtime(&t);

    char buffer[30];
    strftime(buffer, 30, "%y%m%d-%H:%M:%S", tm_info);

    FILE *log = fopen("/home/raditsoic/fs_module.log", "a");
    if (log != NULL) {
        fprintf(log, "%s::%s::%s::%s\n", level, buffer, action, desc);
    }
    fclose(log);
}

static void modular_file(char *path)
{
    FILE *file = fopen(path, "rb");
    int count = 0;
    char tarpath[2000];

    sprintf(tarpath, "%s.%03d", path, count);
    void *buffer = malloc(1024);

    while (1)
    {
        size_t size = fread(buffer, 1, 1024, file);
        if (!size) break;

        FILE *fetch = fopen(tarpath, "w");
        fwrite(buffer, 1, size, fetch);
        fclose(fetch);
        count++;
        sprintf(tarpath, "%s.%03d", path, count);
    }
    free(buffer);
    fclose(file);
    remove(path);
}

static void unmod_dir(char *path)
{
    DIR *dir = opendir(path);
    struct dirent *entry;
    struct stat sb;
    while ((entry = readdir(dir)))
    {
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

        char tarpath[300];
        snprintf(tarpath, sizeof(tarpath), "%s/%s", path, entry->d_name);
        if (stat(tarpath, &sb) == -1) continue;

        if (S_ISDIR(sb.st_mode))
        {
            unmod_dir(tarpath);
        }
        else if (S_ISREG(sb.st_mode) && strlen(tarpath) > 3 && !strcmp(tarpath + strlen(tarpath) - 3, "000"))
        {
            int count = 0;
            char dest[300], temp[400];
            memset(dest, '\0', sizeof(dest));
            memset(temp, '\0', sizeof(temp));
            strncpy(dest, tarpath, strlen(tarpath) - 4);
                
            while(1)
            {
                if (count < 10) snprintf(temp, sizeof(temp), "%s.00%d", dest, count);
                else if (count < 100) snprintf(temp, sizeof(temp), "%s.0%d", dest, count);
                else snprintf(temp, sizeof(temp), "%s.%d", dest, count);

                if (stat(temp, &sb)) break;
                FILE *destfile = fopen(dest, "ab");
                FILE *tempfile = fopen(temp, "rb");
                if (destfile && tempfile) 
                {
                    char buffer[1024];
                    size_t read_bytes;
                    while ((read_bytes = fread(buffer, 1, sizeof(buffer), tempfile)) > 0) 
                    {
                        fwrite(buffer, 1, read_bytes, destfile);
                    }
                }
                if (tempfile) fclose(tempfile);
                if (destfile) fclose(destfile);
                remove(temp);
                count++;
            }
        }
    }
    closedir(dir);
}

static void modular_dir(char *path)
{
    struct dirent *ep;
    DIR *dp; 
    dp = opendir(path);

    if(!dp) return;

    char dir[1000], file[1000];

    while ((ep = readdir (dp)))
    {
      if (!strcmp(ep->d_name, ".") || !strcmp(ep->d_name, "..")) continue;
      if(ep->d_type == DT_DIR)
      {
        sprintf(dir, "%s/%s", path, ep->d_name);
        modular_dir(dir);
      }
      else if (ep->d_type == DT_REG)
      {
        sprintf(file, "%s/%s", path, ep->d_name);
        modular_file(file);
      }
    }
    closedir(dp);
}

static int mount_getattr(const char *path, struct stat *stbuf)
{
	char *modular = strstr(path, "module_");

    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", mounted_path, path);
    res = lstat(fpath, stbuf);

    if (res == -1) 
    {
            if (!modular || !strstr(modular, "/")) 
            {
            return -errno;
            } 
            else 
            {
            sprintf(fpath, "%s%s.000", mounted_path, path);
            lstat(fpath, stbuf);

            int count = 0, sizeCount = 0;
            struct stat st;

            while(!stat(fpath, &st)) 
            {
                count++;
                sprintf(fpath, "%s%s.%03d", mounted_path, path, count);
                sizeCount += st.st_size;
            }
            stbuf->st_size = sizeCount;
        }
    } 
}

static int mount_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) 
{
    char *modular = strstr(path, "module_");
    
    char fpath[1000];
    if (!strcmp(path, "/")) 
    {
        path = mounted_path;
        sprintf(fpath, "%s", path);
    } 
    else 
    {
        sprintf(fpath, "%s%s", mounted_path, path);
    }

    int res = 0;

    struct dirent *ep;
    (void) offset;
    (void) fi;
    DIR *dp;
    dp = opendir(fpath);
    if (!dp) return -errno;

    while ((ep = readdir(dp))) 
    {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = ep->d_ino;
        st.st_mode = ep->d_type << 12;

        int d_len = strlen(ep->d_name);
        if (modular && ep->d_type == DT_REG) 
        {
        if (!strcmp(ep->d_name+(d_len-4), ".000")) 
        {
            ep->d_name[d_len-4] = '\0';
            res = (filler(buf, ep->d_name, &st, 0));
        }
        } 
        else 
        {
        res = (filler(buf, ep->d_name, &st, 0));
        } 
        if (res != 0) break;
    }

    closedir(dp);
    if (res == -1) return -errno;
    make_log("REPORT", "LS", fpath);
    return 0;
}

static int mount_mkdir(const char *path, mode_t mode) {
    char fullpath[MAX_PATH];
    sprintf(fullpath, "%s%s", mounted_path, path);

    int res = mkdir(fullpath, mode);
    if (is_modular_dir(path)) {
        modular_dir(fullpath);
    }

    make_log("REPORT", "CREATE", fullpath);
    return 0;
}

static int mount_rmdir(const char *path) {
    int ismodule = is_modular_dir(path);

    char fullpath[MAX_PATH];
    sprintf(fullpath, "%s%s", mounted_path, path);

    int res = rmdir(fullpath);
    if (res == -1) {
        return -errno;
    }

    if (ismodule) {
        modular_dir(fullpath);
    }

    make_log("FLAG", "RMDIR", fullpath);
    return 0;
}

static int mount_mknod(const char *path, mode_t mode, dev_t rdev)
{   
    char fullpath[MAX_PATH];
    sprintf(fullpath, "%s%s", mounted_path, path);

    int res;
    if (S_ISREG(mode)) {
        res = open(fullpath, O_CREAT | O_WRONLY, mode);
        if (res == -1) {
            return -errno;
        }
        close(res); 
    } else if (S_ISFIFO(mode)) {
        res = mkfifo(fullpath, mode);
    } else {
        res = mknod(fullpath, mode, rdev);
    }

    if (res == -1) {
        return -errno;
    }
        
    make_log("REPORT", "CREATE", fullpath);
    return 0;
}


static int mount_unlink(const char *path) {
    char fullpath[MAX_PATH];
    sprintf(fullpath, "%s%s", mounted_path, path);

    int res = unlink(fullpath);
    if (res == -1) {
        return -errno;
    }

    make_log("FLAG", "UNLINK", fullpath);
    return 0;
}

static int mount_access(const char *path, int mask)
{
	char fullpath[MAX_PATH];
    sprintf(fullpath, "%s%s", mounted_path, path);
    
    int res = access(fullpath, mask);
    if (res == -1) {
        return -errno;
    }

    char desc[512];
    sprintf(desc, "%s-%d", fullpath, mask);

    make_log("REPORT", "ACCESS", desc);
    return 0;
}

static int mount_chmod(const char *path, mode_t mode)
{
    char fullpath[MAX_PATH];
    sprintf(fullpath, "%s%s", mounted_path, path);

    int res = chmod(fullpath, mode);
    if (res == -1) {
        return -errno;
    }

    char desc[512];
    sprintf(desc, "%s::%o", fullpath, mode);

    make_log("REPORT", "CHMOD", desc);
    return 0;
}

static int mount_rename(const char *from, const char *to) {
    int modularfrom = is_modular_dir(from);
    int modularto = is_modular_dir(to);

    char ffrompath[MAX_PATH], ftopath[MAX_PATH];
    sprintf(ffrompath, "%s%s", mounted_path, from);
    sprintf(ftopath, "%s%s", mounted_path, to);

    int res = rename(ffrompath, ftopath);
    if (res == -1) {
        return -errno;
    }

    if (!modularfrom && modularto) {
        modular_dir(ftopath);
    } else if (modularfrom && !modularto) {
        unmod_dir(ftopath);
    }

    char desc[512];
    sprintf(desc, "%s::%s", ffrompath, ftopath);

    make_log("REPORT", "RENAME", desc);
    return 0;
}

static int mount_open(const char *path, struct fuse_file_info *fi)
{   
    char fullpath[MAX_PATH];
    sprintf(fullpath, "%s%s", mounted_path, path);

	int res = open(fullpath, fi->flags);
	if (res == -1)
		return -errno;

	close(res);
    make_log("REPORT", "OPEN", fullpath);
	return 0;
}

static int mount_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{   
    char fullpath[MAX_PATH];
    sprintf(fullpath, "%s%s", mounted_path, path);

	(void) fi;
	int fd = open(fullpath, O_RDONLY);
	if (fd == -1)
		return -errno;

	int res = pread(fd, buf, size, offset);
	if (res == -1)
		res = -errno;

	close(fd);
    make_log("REPORT", "READ", fullpath);
    return res;
}

static int mount_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{   
    char fullpath[MAX_PATH];
    sprintf(fullpath, "%s%s", mounted_path, path);

    (void) fi;
    int fd = open(fullpath, O_WRONLY | O_CREAT, 0666);  // Added O_CREAT flag to create the file if it doesn't exist
    if (fd == -1) {
        return -errno;
    }
        
    int res = pwrite(fd, buf, size, offset);
    if (res == -1) {
        res = -errno;
    }
        
    close(fd);
    make_log("REPORT", "WRITE", fullpath);
    return res;
}


static struct fuse_operations mount_oper = {
    .getattr = mount_getattr,
    .readdir = mount_readdir,
    .rename = mount_rename,
    .unlink = mount_unlink,
    .rmdir = mount_rmdir,
    .mkdir = mount_mkdir,
    .mknod = mount_mknod,
    .open = mount_open,
    .read = mount_read,
    .write = mount_write,
    .access = mount_access,
    .chmod = mount_chmod,
};

int main(int argc, char *argv[])
{
    return fuse_main(argc, argv, &mount_oper, NULL);
}